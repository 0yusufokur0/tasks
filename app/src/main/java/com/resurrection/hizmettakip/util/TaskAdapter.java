package com.resurrection.hizmettakip.util;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.resurrection.hizmettakip.R;
import com.resurrection.hizmettakip.data.db.entity.TaskEntity;

import java.util.ArrayList;
import java.util.List;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.TaskHolder> {

    private List<TaskEntity> taskEntities = new ArrayList<>();
    private OnItemClickListener listener;
    private OnItemLongClickListener onItemLongClickListener;
    public boolean isLongPressed = false;
    public ArrayList<TaskEntity> selectedItems = new ArrayList<TaskEntity>();

    @NonNull
    @Override
    public TaskHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.task_item, parent, false);


        return new TaskHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskHolder holder, int position) {
        TaskEntity currentTask = taskEntities.get(position);
        holder.textViewTitle.setText("currentTask.getTask()");
        holder.textViewDescription.setText("(int) currentTask.getId()");
        if (isLongPressed) {
            holder.checkBox.setVisibility(View.VISIBLE);
        } else {
            holder.checkBox.setVisibility(View.GONE);

        }


    }

    @Override
    public int getItemCount() {
        return taskEntities.size();
    }

    public void setTask(List<TaskEntity> taskEntities) {
        this.taskEntities = taskEntities;
        notifyDataSetChanged();
    }

    public TaskEntity getTask(int position) {
        return taskEntities.get(position);
    }

    class TaskHolder extends RecyclerView.ViewHolder {
        private TextView textViewTitle;
        private TextView textViewDescription;
        private CheckBox checkBox;

        public TaskHolder(@NonNull View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.textViewTitle);
            textViewDescription = itemView.findViewById(R.id.textViewDescription);
            checkBox = itemView.findViewById(R.id.checkBox);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (isLongPressed) {
                        if (!checkBox.isChecked()) {
                            checkBox.setChecked(true);
                            selectedItems.add(taskEntities.get(position));
                            textViewTitle.setTextColor(Color.parseColor("#ffffff"));

                        } else {
                            checkBox.setChecked(false);
                            selectedItems.remove(taskEntities.get(position));
                            textViewTitle.setTextColor(Color.parseColor("#0593c5"));


                        }
                    } else {
                        if (listener != null && position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(taskEntities.get(position));


                        }
                    }

                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    int position = getAdapterPosition();
                    if (onItemLongClickListener != null && position != RecyclerView.NO_POSITION) {
                        onItemLongClickListener.onItemLongClick(taskEntities.get(position));
                        textViewTitle.setTextColor(Color.parseColor("#ffffff"));
                        isLongPressed = true;
                        notifyDataSetChanged();
                        checkBox.setChecked(true);
                        selectedItems.add(taskEntities.get(position));


                    }

                    return false;
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(TaskEntity taskEntities);
    }

    public interface OnItemLongClickListener {
        void onItemLongClick(TaskEntity taskEntity);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener onItemLongClickListener) {
        this.onItemLongClickListener = onItemLongClickListener;

    }


}
