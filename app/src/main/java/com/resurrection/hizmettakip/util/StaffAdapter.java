package com.resurrection.hizmettakip.util;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.resurrection.hizmettakip.R;
import com.resurrection.hizmettakip.data.db.entity.StaffEntity;

import java.util.ArrayList;
import java.util.List;

public class StaffAdapter extends RecyclerView.Adapter<StaffAdapter.StaffHolder> {


    private List<StaffEntity> staffEntities = new ArrayList<>();
    private OnItemClickListener listener;

    @NonNull
    @Override
    public StaffHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.staff_item,parent,false);
        return new StaffHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StaffHolder holder, int position) {
        StaffEntity currentStaffEntity = staffEntities.get(position);
        holder.name.setText(currentStaffEntity.getName().toString());
        holder.surname.setText(currentStaffEntity.getSurname().toString());



    }

    @Override
    public int getItemCount() {
        return staffEntities.size();
    }
    public StaffEntity getStaff(int position){
        return staffEntities.get(position);
    }
    public void setStaff(List<StaffEntity> staffEntities){
        this.staffEntities = staffEntities;
        notifyDataSetChanged();
    }


    class StaffHolder extends RecyclerView.ViewHolder{
        private TextView name,surname;

        public StaffHolder(@NonNull View itemView) {
            super(itemView);
            name = (TextView)itemView.findViewById(R.id.name);
            surname =(TextView) itemView.findViewById(R.id.surname);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if(listener != null && position != RecyclerView.NO_POSITION)
                    {
                        listener.onItemClick(staffEntities.get(position));
                    }
                }
            });


        }
    }

       public interface OnItemClickListener {
        void onItemClick(StaffEntity staffEntity);
    }

    public void setOnItemClickListener(OnItemClickListener listener)
    {
        this.listener = listener;
    }

}
