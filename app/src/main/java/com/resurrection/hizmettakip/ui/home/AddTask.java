package com.resurrection.hizmettakip.ui.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.resurrection.hizmettakip.R;
import com.resurrection.hizmettakip.data.db.entity.StaffEntity;
import com.resurrection.hizmettakip.ui.base.TaskViewModel;
import com.resurrection.hizmettakip.util.StaffAdapter;

import java.util.List;

public class AddTask extends AppCompatActivity {

    EditText title;
/*
    EditText description;
*/
    Button cancel;
    Button save;
    private StaffAdapter staffAdapter = new StaffAdapter();
    private TaskViewModel taskViewModel;

    private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        init();
        getAndSetItems();
        getSupportActionBar().setTitle("Add Task");

        title = findViewById(R.id.editTextTitle);
/*
        description = findViewById(R.id.editTextDescription);
*/
        cancel = findViewById(R.id.buttonCancel);
        save = findViewById(R.id.buttonSave);


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "kaydedilmedi", Toast.LENGTH_LONG).show();
                finish();

            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveTodo();
            }
        });


    }
    private void init(){
        recyclerView = findViewById(R.id.addTaskStaffRecyclerview);
    }
    public void saveTodo()
    {
        String TodoTitle = "asdasd";
        String TodoDescription = "description";
        Intent i = new Intent();
        i.putExtra("todoTitle", TodoTitle);
        i.putExtra("todoDescription", TodoDescription);
        setResult(RESULT_OK, i);
        finish();

    }
    private void getAndSetItems() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(staffAdapter);
        taskViewModel = ViewModelProviders.of(this).get(TaskViewModel.class);

        taskViewModel.getAllStaff().observe(this, new Observer<List<StaffEntity>>() {
            @Override
            public void onChanged(List<StaffEntity> staffEntities) {
                staffAdapter.setStaff(staffEntities);
            }
        });

    }



}

































